function test_plotting() {
  // hmm. how do i know the name of the site?
  // this is not an issue because it will be called only from the console,
  // it's for testing only.
  jQuery.getJSON("/sites/ironman.teamchad.ca/files/garmin_connector/activities/plot-data/403540379.json",function(data) {
    // console.log("Done! Here's the data" + data.sport);
    csm_plot(data);
  });
}

function garmin_connector_create_plot(url,destination) {
  if (destination == "") { destination = "#single-plot-container"; }
    jQuery.getJSON(url,function(data) {
      csm_plot(data,destination);
  });
}

var csmplots = [];
function csm_plot(data,destination,hide) {
  if (hide) {
    console.log("You want to hide this series: " + hide);
  }
  var sport = data.summary.activityType.parent.key;
  var id = data.summary.activityId;
  var second_axes = {};
  var hr = [];
  var second_y = {};
  var hrfound = 0;
  if (data.heartrate && data.heartrate.data) {
    var debug_hr = 1;
    var low_index = data.heartrate.data[0][0];
    var high_index;
    jQuery.each(data.heartrate.data,function(index,value) {
        hr[index] = value;
        high_index = index;
    });
    hr = garmin_connector_repair_array(hr,low_index,high_index);
    hrfound = 1;
  }
  else {
    var message = "Heart rate data is absent for the activity with id (";
    message += id + "). You could first check this with ";
    message += "garmin connect http://connect.garmin.com/activity/" + id;
    message += ". You may just have not worn a strap on that day.";
    // You don't know that console is available. This is left here in case I
    // decide to create a module dependency on Firebug Lite
    // console.log(message);
  }
  if (sport == "cycling") {
    var power = [];
    if (data.power) {
      if (data.power.length == 0) {
        var message = "Although the device was set up to record power, no ";
        message += "power was ever recorded for activity with id(" + id;
        message += "). This can happen if the activity is really short. ";
        message += "Perhaps you can purge this activity from the activity ";
        message += "catalog.";
        // You don't know that console is available. This is left here in case I
        // decide to create a module dependency on Firebug Lite
        // console.log(message);
      }
      else {
        jQuery.each(data.power.data,function(index,value) {
          power[index] = value;
        });
      }
    }
    else {
      var message = "There is no power data for the cycling activity with id ";
      // You don't know that console is available. This is left here in case I
      // decide to create a module dependency on Firebug Lite
      // console.log(message + id);
    }
    second_axes = {
      data: power,
      color: "black",
      label: "Power",
      yaxis: 2
    };
    second_y = {
      axisLabel: "Power (W)",
      position: "right",
      min: 80,
      ticks: 10,
      tickFormatter: powerFormatter,
    };
  }
  else if (sport == "running") {
    // var debug = 1;
    var pace = [];
    var message = "";
    if (data.pace) {
      // There were circumstances where garmin connect did not.
      // Provide the pace.
      if (data.pace.data) {
        var low_index = data.pace[0];
        var high_index;
        jQuery.each(data.pace.data,function(index,value) {
          pace[index] = value;
          high_index = index;
        });
        pace = garmin_connector_repair_array(pace, low_index, high_index);
        second_axes = {
          data: pace,
          color: "gray",
          label: "Pace",
          lines: { show: true },
          yaxis: 2,
        };
        second_y = {
          min: 3,
          max: 9,
          panRange: null,
          zoomRange: [1,1],
          axisLabel: "Pace (" + data.pace.units + ")",
          position: "right",
          ticks: [[3,"3:00"],[3.5,"3:30"],[4,"4:00"],[4.5,"4:30"],[5,"5:00"],[5.5,"5:30"],[6,"6:00"],[6.5,"6:30"],[7,"7:00"],[7.5,"7:30"],[8,"8:00"],[8.5,"8:30"],[9,"9:00"],[9.5,"9:30"],[10,"10:00"]],
          // tickFormatter: paceFormatter,
          transform: function (v) { return -v; },
          inverseTransform: function (v) { return -v; }
      };
      }
      else {
        debug && console.log("Unfortunately, data.pace.data was not found for id " + id);
      }
    }
    else {
      message = "Although " + id + " is a running activity, no pace was found. ";
      message += "This could have happened becase your GPS was turned off ";
      message += "or because you were running on a treadmill with no footpod.";
      debug && console.log(message);
    }
}

function garmin_connector_repair_array(csm,low,high) {
  var previous_value = 0;
  // Sometimes, data is missing. fill it in.
  for (index = 0; index <= high; index++) {
    if (csm[index] === undefined) { csm[index] = previous_value; }
    else { previous_value = csm[index]; }
  }
  return csm;
}

function doPlot(position,destination,hide) {
  // .plot(destination,data,options
  csmplots[destination] = jQuery.plot(jQuery(destination + " .plot-window"),
    [ { data: hr,
          color: "red",
          label: "Heart Rate"
          },
          second_axes
    ],
      // plot options
    {
      points: { show: false, fill: false },
      lines:  { show: true, fill: false },
      xaxes: [ {
        show: true,
        position: "bottom",
        axisLabel: "Time",
        tickFormatter: timeFormatter,
        minTickSize: 600,
        zoomRange: [-100, 10000],
        panRange: [-400, 10000],
        font: {
          size: 18,
          style: "italic",
          weight: "bold",
          family: "sans-serif",
          variant: "small-caps"
        },
        min: -400,
        max: null,
      } ],
      yaxes: [ {
        min: 100,
        max: 200,
        // this does actually work here
        zoomRange: [1, 1],
        panRange: [100, 200],
        axisLabel: "Heart Rate",
        tickFormatter: hrFormatter },
        second_y
      ],
      legend: {
        position: 'nw',
        labelFormatter: labelFormatter,
        },
        zoom: {
        interactive: true
        },
        pan: {
        interactive: true
      }
    });
    csmplots[destination].destination = destination;
  }
  doPlot("right",destination);
  jQuery(destination + " .title").html(data.summary.activityName);
  jQuery(destination + " .title").addClass(sport);
  summary = "";
  type = data.summary.activityType.parent.key;
  summary += "<div class='name " + type + "'>" + data.summary.activityName + "</div>";
  summary += garmin_connector_get_activity_summary(data.summary);
  summary += "<div class='gc-link'><a class='player-link' ";
  summary += "href='http://connect.garmin.com/player/";
  summary += data.summary.activityId + "'>View in Garmin Connect</a></div>";
  jQuery(destination + " .activity-summary").html(summary);
  jQuery('.legendColorBox').click(function() {
    // selected = jQuery(this.siblings().text());
  });
}

function ISODateString(d){
  function pad(n){return n < 10 ? '0' + n : n}
  return d.getUTCFullYear() +
    '-' +
    pad(d.getMonth() + 1) +
    '-' + pad(d.getDate()) +
    ' ' + pad(d.getHours()) +
    ':' + pad(d.getMinutes());
}

function garmin_connector_get_activity_summary(summary) {
  var type = summary.activityType.parent.key;
  returner = "<div class='summary " + type + "'>";
  var d = new Date(summary.activitySummary.BeginTimestamp.value);
  returner += "<div class='date'><div class='field-title'>Date</div> ";
  returner += ISODateString(d) + "</div>";
  returner += "<div class='duration'><div class='field-title'>Duration</div> ";
  returner += summary.activitySummary.SumDuration.display + "</div>";
  if (summary.activitySummary.SumTrainingEffect) {
    returner += "<div class='te'><div class='field-title'>Sum Training Effect</div> ";
    returner += summary.activitySummary.SumTrainingEffect.display + "</div>";
  }
  if (type == "cycling") {
    if (summary.activitySummary.MaxPowerTwentyMinutes) {
      returner += "<div class='max-power'><div class='field-title'>Max Power 20 minutes</div> ";
      returner += summary.activitySummary.MaxPowerTwentyMinutes.withUnit + "</div>";
    }
    if (summary.activitySummary.WeightedMeanNormalizedPower) {
      returner += "<div class='normalized-power'><div class='field-title'>";
      returner += "Weighted Normalized Power</div> ";
      returner += summary.activitySummary.WeightedMeanNormalizedPower.withUnit + "</div>";
    }
    if (summary.activitySummary.SumTrainingStressScore) {
      returner += "<div class='sum-tss'><div class='field-title'>Sum TSS:</div> ";
      returner += summary.activitySummary.SumTrainingStressScore.display + "</div>";
    }
    if (summary.activitySummary.SumIntensityFactor) {
      returner += "<div class='sum-if'><div class='field-title'>Sum Intensity Factor:</div> ";
      returner += summary.activitySummary.SumIntensityFactor.display + "</div>";
    }
  }
  var pat = new RegExp("Z2|Zone 2|Long");
  if (pat.test(summary.activityName)) {
    returner += "<div class='hr'><div class='field-title'>";
    returner += "Zone 2 Average Heart Rate:</div> ";
    returner += summary.activitySummary.WeightedMeanHeartRate.withUnitAbbr;
    returner += "</div>";
  }
  returner += "<div class='activity-description'><div class='field-title'>";
  returner += "Comment</div><div class='field-data'>";
  returner += summary.activityDescription + "</div></div>";
  returner += "</div>";
  return returner;
}

function powerFormatter(v, axis) {
   // return v.toFixed(axis.tickDecimals) +" Watts";
   // we don't really need this but I left it here as a demo
   return v.toFixed(axis.tickDecimals);
}
function paceFormatter(v, axis) {
  // minperkm = 16.6666666/v;
  // from meters per second, calculate meters/km with 1/(.06x)
  // however, it seems that
  minutes = Math.floor(v);
  seconds = v / 60;
  var ss = seconds.toString();
  rounded = ss.replace(/\d+/g, function(m){
    return "0".substr(ss.length - 1) + m;
  });
  returner = minutes + ":" + rounded;
  // returner = minperkm.toFixed(2);
  return returner;
}
function hrFormatter(v, axis) {
   return v.toFixed(axis.tickDecimals);
}

function labelFormatter(label,series) {
  return '<a onclick="garmin_connector_toggle_series(jQuery(this)); ' +
    'return false;">' + label + '</a>';
}

function garmin_connector_show_time(object,time) {
  var parent = jQuery(object).parents(".plot-container").attr("id");
  var plot = csmplots["#" + parent];
  var xax = plot.getAxes().xaxis;
  var data = plot.getData();
  if (time == "all") {
    // xax.panrange = [-400,xax.datamax];
    // xax.max = xax.datamax;
    plot.zoomOut();
    // plot.zoom({ amount: .1, center: { left: 500 }});
  }
  else {
    plot.zoom();
    // plot.zoom({ amount: 25, center: { left: 100} });
    // xax.max = 1000;
    // console.log("Setting timeframe to 10 minutes");
  }
  plot.setupGrid();
  plot.draw();
}

function garmin_connector_find_series(plot,title) {
  var data = plot.getData();
  for (i = 0; i < data.length; i++) {
    var thisone = data[i];
    if (thisone.label == title) {
      return thisone;
    }
  }
}

function garmin_connector_toggle_series(object) {
  var parent = jQuery(object).parents(".plot-container").attr("id");
  var plot = csmplots["#" + parent];
  var series_title = jQuery(object).html();
  var series = garmin_connector_find_series(plot,series_title);
  var show = series.lines.show;
  if (series.lines.show == false) {
    series.lines.show = true;
  }
  else if (series.lines.show == true) {
    series.lines.show = false;
  }
  plot.draw();
}

function timeFormatter(v,axis) {
  minutes = Math.floor(v / 60);
  seconds = v % 60;
  var ss = seconds.toString();
  rounded = ss.replace(/\d+/g, function(m){
    return "0".substr(ss.length - 1) + m;
  });
  return minutes + ":" + rounded;
}

function trigger_purge_workout(id) {
  jQuery.ajax({
    url: "/garmin-connector/purge-workout.html?id=" + id,
    success: function(data,textStatus,jqXHR) {
      // When there is no console, how do you notify?
      // console.log("Ajax complete with data " + data);
    },
    error:   function(jqXHR, textStatus, errorThrown) {
      // When there is no console, how do you notify?
      // console.log("Ajax failed with text status (" + textStatus + ")");
    }
  });
  jQuery("#last-ten-" + id).remove();
  jQuery(".rendered-workout-" + id).remove();
}

function toggle_workout_type_other() {
  jQuery(".workout-manager-row.other").toggle();
}

function garmin_connector_toggle_section(section) {
  jQuery("." + section + " .view-content").toggle();
  jQuery("." + section + " .view-footer").toggle();
  if (jQuery("." + section + " .view-content").is(":hidden")) {
    jQuery("." + section + " .view-header .show").css("display","block");
    jQuery("." + section + " .view-header .hide").css("display","none");
  }
  else {
    jQuery("." + section + " .view-header .show").css("display","none");
    jQuery("." + section + " .view-header .hide").css("display","block");
  }
}
// vim: ts=2 shiftwidth=2
