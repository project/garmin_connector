<?php
/**
 * @file
 * API documentation for garmin_connector hooks
 */

/**
 * Respond to the creation of a new strength activity on Garmin Connect.
 *
 * An example of this is implemented in garmin_connector_post_strength_upload.
 *
 * @param int $id
 *   The Garmin Connect id of the newly created workout
 *
 * @param string $type
 *   The type of the activity. Likely one of 'strength' or corrective exercises
 *
 * @param string $title
 *   The new title that you'd like the workout to be called. $title will be
 *   sanitized in this function so do not do that before hand.
 *
 * @return array
 *   A drupal render array with the status of any operations.
 *
 * @see garmin_connector_post_activity_upload()
 */
function hook_garmin_connector_post_activity_upload($id = NULL, $type = "unknown", $title = NULL) {
}

/**
 * Respond to the creation of a form listing strength workouts.
 *
 * An example of this is implemented in garmin_connector_strength_workout_types.
 *
 * @return array
 *   An array with a list or workout types to be rendered as form options.
 *
 * @see garmin_connector_strength_workout_types()
 */
function hook_garmin_connector_strength_workout_types() {
}
