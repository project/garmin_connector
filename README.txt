
garmin_connector

This project uses the Garmin Connect REST API to cache your workout data from
Garmin Connect. This includes downloading all of your TCX workout summary files
and summaries in the form of JSON files.

Once the files have been cached, a number of interesting data summary and
presentation functions are provided:
1. A block that lists your last X key workouts
2. A block that rewards you for completing workouts that you hate
3. A "quick view" of pace, heart rate, power, and other workout metrics
4. A calendar-type view that summarizes this month's and last month's workout
 activity

Why cache the data?
1. Provide you with a backup of all of your workout data
2. Performance using the REST API alone provided very poor and unreliable
 performance

Be sure to configure your Garmin Connect username and password in
admin/config/garmin_connector/garmin_connector after you install this module!


GETTING STARTED

1. Install and enable the module
2. Configure your account information at
    admin/config/garmin_connector/garmin_connector
3. Retreieve some activities from your Garmin Connect account.
    I do this with drush:
     drush -r $DRUPAL_ROOT -l $WEBSITE eval
     'garmin_connector_retrieve_activities()'
3. Turn on some blocks in block settings
    (garmin connector blocks are obviously labeled)

Start with the page here:
/garmin-connector/garmin-connector.html

IF YOU DON'T HAVE A GARMIN CONNECT ACCOUNT

You can install sample data using drush something like this:
$ sudo -u www-data drush -r /var/lib/drupal7-garminconnector
garmin-connector-sample-data
Note that this assumes LAMP and the apache user www-data. It's ok to not sudo
but remember to change permissions when required.

Look at some of the code that generates that page.

Additional Notes

PHP's curl library is required.

There are a variety of utility methods implemented for drush. run drush help to
get a list of those methods.

You can perform a variety of system catalogue-related tasks at the url:
garmin-connector/catalogue-maintenance.html

As of version 7.x-1.0, Garmin Connector is designed to allow one Garmin Connect
user per drupal installation. A roadmap exists for extending Garmin Connector to
multiple users in future versions.



// vim: tw=80
