<?php
/**
 * @file
 * The main module
 */

/**
 * Implements hook_permission().
 */
function garmin_connector_permission() {
  return array(
    'manage activities' => array(
      'title'       => t('Manage activities'),
      'description' => t('The user can add and remove activities from the filesystem and from the system catalogue.'),
    ),
    'create correctional exercises' => array(
      'title'       => t('Create Correctional Exercises.'),
      'description' => t('The user can create a workout on Garmin Connect from the page exercises.html. In Chads case these are called "Correctional Exercises". The reason why you want to do this is because it is easier to press a button in Garmin Connector than it is to go to Garmin Connect and manually create an activity that corresponds to a correctional exercies.'),
    ),
    'upload strength workouts' => array(
      'title'       => t('Upload Strength Workouts to Garmin Connect'),
      'description' => t('The user can upload strength workouts to Garmin Connect.'),
    ),
    'maintain activity sets' => array(
      'title'       => t('Maintain Activity Sets'),
      'description' => t('The user can create and modify activity sets.'),
    ),
    'view activity sets' => array(
      'title'       => t('View Activity Sets'),
      'description' => t('The user can view activity sets.'),
    ),
    'view activities' => array(
      'title'       => t('View Activities'),
      'description' => t('The user can view the graphs of activities'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function garmin_connector_menu() {
  $items = array();
  $items['admin/config/garmin_connector'] = array(
    'title'             => 'Garmin Connector configuration',
    'description'       => 'This is the parent item for administration of Garmin Connector',
    'position'          => 'left',
    'page callback'     => 'system_admin_menu_block_page',
    'access arguments'  => array('administer site configuration'),
    'file'              => 'system.admin.inc',
    'file path'         => drupal_get_path('module', 'system'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['admin/config/garmin_connector/garmin_connector'] = array(
    'title'             => 'Garmin Connector Settings',
    'description'       => 'Setup the Garmin Connector',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('garmin_connector_admin'),
    'file'              => 'garmin_connector.pages.inc',
    'access arguments'  => array('administer garmin_connector settings'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/compare.html'] = array(
    'title'             => 'Compare activities',
    'description'       => 'Render several activities near each other so that they can be compared.',
    'file'              => 'garmin_connector.pages.inc',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('garmin_connector_compare_activities_form'),
    'access arguments'  => array('view activities'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/purge.html'] = array(
    'title'             => 'Purge activities',
    'description'       => 'Purge activities from the filesystem.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('garmin_connector_purge_activities_form'),
    'file'              => 'garmin_connector.pages.inc',
    'access arguments'  => array('manage activities'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/garmin-connector.html'] = array(
    'title'             => "Garmin Connector Demonstration Page",
    'description'       => 'Demonstrate the features of Garmin Connector',
    'page callback'     => 'garmin_connector_show_demo',
    'file'              => 'garmin_connector.pages.inc',
    'access callback'   => TRUE,
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/activity-sets.html'] = array(
    'title'             => 'Garmin Connector Activity Set Viewer',
    'description'       => 'View Activity Sets.',
    'page callback'     => 'drupal_get_form',
    'file'              => 'garmin_connector.pages.inc',
    'page arguments'    => array('garmin_connector_activity_set_form'),
    'access callback'   => TRUE,
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/activity-manager.html'] = array(
    'title'             => "Activity Manager",
    'description'       => 'Manage your activities - add, purge, add to workout sets.',
    'page callback'     => 'garmin_connector_activity_manager',
    'file'              => 'garmin_connector.pages.inc',
    'access arguments'  => array('manage activities'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/last-ten-hard/bike.html'] = array(
    'title'             => "Last ten hard bike workouts",
    'description'       => 'Last Ten Hard Bike Workouts. Useful for meetings with coaches.',
    'access callback'   => TRUE,
    'file'              => 'garmin_connector.pages.inc',
    'page callback'     => 'garmin_connector_last_ten_hard',
    'page arguments'    => array('cycling'),
    'type'              => MENU_NORMAL_ITEM,
  );
  $items['garmin-connector/last-ten-hard/run.html'] = array(
    'title'             => "Last ten hard run workouts",
    'description'       => 'Last Ten Hard Run Workouts. Useful for meetings withcoaches.',
    'access callback'   => TRUE,
    'file'              => 'garmin_connector.pages.inc',
    'page callback'     => 'garmin_connector_last_ten_hard',
    'page arguments'    => array('running'),
    'type'              => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_block_info().
 */
function garmin_connector_block_info() {
  $blocks['last-ten-non-z2-workouts'] = array(
    'info'    => t('Garmin Connector Last Ten Key Activities'),
    'cache'   => 'DRUPAL_CACHE_GLOBAL',
  );
  $blocks['quick-view-result-pane'] = array(
    'info'    => t('Garmin Connector Quick View Pane'),
    'cache'   => 'DRUPAL_CACHE_GLOBAL',
  );
  $blocks['reward-zone'] = array(
    'info'    => t('Garmin Connector Reward Pane'),
    'cache'   => 'DRUPAL_CACHE_GLOBAL',
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function garmin_connector_block_view($delta) {
  $block = array();
  module_load_include('inc', 'garmin_connector', 'garmin_connector.pages');
  $found = FALSE;
  if ($delta == "last-ten-non-z2-workouts") {
    $found = TRUE;
    // These are the key workouts.
    $activities = garmin_connector_get_activities_from_local();
    $block['workouts'] = garmin_connector_get_last_ten_activities($activities);
    $block['#theme'] = "garmin_connector_block_wrapper";
  }
  if ($delta == "quick-view-result-pane") {
    $found = TRUE;
    $latest_run = garmin_connector_get_latest("running|cycling");
    $block['result-pane'] = garmin_connector_get_plot_container("0", $latest_run->activityId);
    $block['#theme'] = "garmin_connector_block_wrapper";
  }
  if ($delta == "reward-zone") {
    $found = TRUE;
    $block['reward-pane'] = garmin_connector_get_reward_pane();
    $block['#theme'] = "garmin_connector_block_wrapper";
  }
  if ($found == TRUE) {
    garmin_connector_add_css_js();
  }
  return $block;
}

/**
 * Implements hook_theme().
 */
function garmin_connector_theme() {
  return array(
    'garmin_connector_block_wrapper' => array(
      'render element'  => 'element',
    ),
    'garmin_connector_contextual_links_wrapper' => array(
      'render element' => 'element',
      'variables' => array(
        'gc-id'     => '',
        'gc-type'   => '',
        'gc-css-id' => '',
        'gc-name'   => '',
        'gc-classes' => array(),
        'markup'    => '',
        'children'  => array(),
      ),
    ),
    'garmin_connector_plot_container' => array(
      'render element'  => 'element',
      'variables'       => array(
        'serial'  => '0',
        'id'      => '',
      ),
    ),
    'garmin_connector_activity_row' => array(
      'render element' => 'element',
      'variables'       => array(
        'date'  => '',
        'duration'  => '',
        'name' => '',
        'gc-id' => '',
        'classes' => array(),
        'children' => array(),
      ),
    ),
  );
}

/**
 * Gets the default configuration for Garmin Connector.
 *
 * There are two methods for dealing with the catalogue: this one returns an
 * array (useful for .install), and the other returns a single key (improves the
 * readability of the code).
 *
 * @return array
 *   Garmin Connector's configuration
 */
function garmin_connector_get_config_array() {
  $gc_root = file_default_scheme() . "://garmin_connector";
  $cf = array(
    'dir_gc'                => $gc_root ,
    'gc_catalogue'          => $gc_root . "/catalogue.json",
    'gc_catalogue_by_date'  => $gc_root . "/catalogue-by-date.json",
    'dir_tcx_swimming'      => $gc_root . "/activities/tcx/swimming",
    'dir_tcx_cycling'       => $gc_root . "/activities/tcx/cycling",
    'dir_tcx_running'       => $gc_root . "/activities/tcx/running",
    'dir_tcx_other'         => $gc_root . "/activities/tcx/other",
    'dir_tcx_fitness_equipment'  =>
    $gc_root . "/activities/tcx/fitness_equipment",
    'dir_details_swimming'  => $gc_root . "/activities/details/swimming",
    'dir_details_cycling'   => $gc_root . "/activities/details/cycling",
    'dir_details_running'   => $gc_root . "/activities/details/running",
    'dir_details_other'     => $gc_root . "/activities/details/other",
    'dir_details_fitness_equipment' =>
    $gc_root . "/activities/details/fitness_equipment",
    'dir_summary'           => $gc_root . "/activities/summary",
    'dir_summary_swimming'  => $gc_root . "/activities/summary/swimming",
    'dir_summary_cycling'   => $gc_root . "/activities/summary/cycling",
    'dir_summary_running'   => $gc_root . "/activities/summary/running",
    'dir_summary_other'     => $gc_root . "/activities/summary/other",
    'dir_summary_fitness_equipment'  =>
    $gc_root . "/activities/summary/fitness_equipment",
    'dir_plot_data'         => $gc_root . "/activities/plot-data",
    'dir_json'              => $gc_root . "/activities/json",
  );
  return $cf;
}

/**
 * Gets a single configuration key.
 *
 * See garmin_connector_get_config_array() for more information on the thinking
 * here.
 *
 * @param string $key
 *   The parameter to retrieve.
 *
 * @return string
 *   The configuration value.
 */
function garmin_connector_get_config($key) {
  $cf = garmin_connector_get_config_array();
  return $cf[$key];
}

/**
 * Implements hook_cron().
 *
 * It is tricky to have a hook_cron for Garmin Connector. Here's why:
 * - cron would not know which user to run as without creating a Role
 *   for Garmin Connector and looping over each user in that role.
 * - Even for one user, the call to Garmin Connect is an expensive operation.
 *   If cron is set to run with a high level of frequency this could cause
 *   problems for the site.
 * 
 * @todo
 * - Create a system of user management for Garmin Connector.
 * - Add an administrative option specifying the minimum time between cron runs
 */
function garmin_connector_cron() {
  // Please read the notes for this method.
}

/**
 * Implements hook_garmin_connector_post_activity_upload().
 *
 * Change the name of a newly created strength activity on Garmin Connect from
 * 'Untitled' to something more logical. Garmin does not allow setting the 
 * name at upload time.
 */
function garmin_connector_garmin_connector_post_activity_upload($id,
     $type, $title) {
  $title = check_plain($title);
  $returner = "";
  // I use cURL here rather than using drupal_http_request because I could not..
  // Get drupal_http_request to work successfully despite trying hard. I also...
  // Resuse the same connection for all methods contacting Garmin Connect...
  // So the most reliable way to make this work is to be consistent.
  $ch = garmin_connector_initialize();
  // Update the name.
  $url = "http://connect.garmin.com/proxy/activity-service-1.2/json/name/$id";
  curl_setopt($ch, CURLOPT_HTTPHEADER, array());
  curl_setopt($ch, CURLOPT_POSTFIELDS, array('value' => $title));
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  $result2 = curl_exec($ch);
  $returner .= "<p>";
  $returner .= t("Result of the name change to !title",
    array(
      '!title' => $title,
    )
  );
  $returner .= "</p><pre>";
  $returner .= check_plain($result2) . "</pre>\n";
  if (!isset($result2) || curl_error($ch)) {
    $returner .=
      t("There was an error changing the name of the newly created activity at Garmin Connect. The Garmin Connect activity id was (@id) and the error was (!error). The URL was (!url)",
        array(
          '!error' => curl_error($ch),
          '@id'    => $id,
          '!url'   => $url,
        )
      );
  }
  else {
    $returner .= "<p>" .
      t("There was success in changing the name of Garmin Connect activityId @id from 'Untitled' to '@title'.",
        array(
          '@id' => $id,
          '@title' => $title,
        )
      ) . "</p>";
  }
  return array(
    'garmin-connector-things' => array(
      '#markup' => $returner,
    ),
  );
}

/**
 * Ajax callback for completed correctional exercises.
 */
function garmin_connector_exercises_done($form, &$form_state) {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  if (user_access("create correctional exercises") == TRUE) {
    $results = garmin_connector_upload_activity(20);
    $invoker = module_invoke_all('garmin_connector_post_activity_upload',
     $results['tcx-uploader']['#id'], 'corrective exercises', "Corrective Exercises");
  }
  else {
    $invoker = array();
    $results = t("Sorry, you didn't have permission to create exercises");
  }
  $all = array_merge($results, $invoker);
  return array(
    '#type'   => 'ajax',
    '#commands'    => array(
      ajax_command_replace(
        ".exercises-done-results",
        "<div class='exercises-done-results'>" . drupal_render($all) . "</div>"),
    ),
  );
}

/**
 * Callback for uploading completed strength exercises.
 *
 * @return array
 *   An ajax array.
 */
function garmin_connector_strength_done($form, &$form_state) {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  if (user_access("upload strength workouts") == TRUE) {
    // Get the exercise type.
    $title = $form_state['values']['type'];
    // Get the exercise duration.
    $duration = $form_state['values']['duration'];
    $results = garmin_connector_upload_activity($duration);
    $invoker = module_invoke_all('garmin_connector_post_activity_upload',
      $results['tcx-uploader']['#id'], 'strength', $title);
  }
  else {
    $invoker = array();
    $results = t("Sorry, you didn't have permission to create exercises");
  }
  $all = array_merge($results, $invoker);
  return array(
    '#type'   => 'ajax',
    '#commands'    => array(
      ajax_command_replace(
        ".strength-done-results",
        "<div class='strength-done-results'>" . drupal_render($all) . "</div>"
      ),
    ),
  );
}

/**
 * Implements hook_help().
 *
 * @todo
 * Correctly format this inline.
 */
function garmin_connector_help($path, $arg) {
  switch ($path) {
    case 'admin/help#garmin_connector':
      // Return a line-break version of the module README.txt
      $contents = file_get_contents(
      drupal_get_path('module', 'garmin_connector') . "/README.txt");
      return check_markup($contents);
  }
}
