<?php
/**
 * @file
 * Drush extensions for Garmin Connector.
 *
 * Contains methods for use with drush.
 */

/**
 * Implements hook_drush_help().
 */
function garmin_connector_drush_help($command) {
  switch ($command) {
    case 'drush:garmin-connector-refresh-activity':
      return t("Refresh an activity from the Garmin Connector filesystem");

    case 'drush:garmin-connector-purge-activity':
      return t("Purge an activity from the Garmin Connector filesystem");

    case 'drush:garmin-connector-list-activities':
      return t("List activities in the garmin connector filesystem");

    case 'drush:garmin-connector-rebuild-system-catalogue':
      return t("Rebuild the garmin connector system catalogue");

    case 'drush:garmin-connector-retrieve':
      return t("Retrieve activities from Garmin Connect");

    case 'drush:garmin-connector-purge-untitled':
      return t("Purge recent workouts that are titled 'Untitled'");

    case 'drush:garmin-connector-sample-data':
      return t("Download sample data");

    case 'drush:garmin-connector-test-login':
      return t("Test if Garmin Connector is logged into Garmin Connect");
  }
}

/**
 * Implements hook_drush_command().
 */
function garmin_connector_drush_command() {
  $items = array();
  $items['garmin-connector-sample-data'] = array(
    'description' => t('Download sample data for Garmin Connector'),
    'arguments' => array(),
    'examples' => array(
      'Standard Example' => 'drush garmin-connector-sample-data',
    ),
  );
  $items['garmin-connector-retrieve'] = array(
    'description' => t('Retrieve activities from Garmin Connect'),
    'arguments' => array(
      'from' => t('The earliest date'),
      'to' => t('The latest date'),
    ),
    'examples' => array(
      'Standard Example' => 'drush garmin-connector-retrieve (defaults to the last 30 days)',
      'Argument example' => 'drush garmin-connector-retrieve 2013-01-01 2013-07-07',
    ),
  );
  $items['garmin-connector-refresh-activity'] = array(
    'description' => t('Refresh an activity by ID'),
    'arguments' => array(
      'id' => t('The activity ID'),
    ),
    'examples' => array(
      'Argument example' => 'drush garmin-connector-refresh-activity 1142234',
    ),
  );
  $items['garmin-connector-purge-activity'] = array(
    'description' => t('Purge a activity by ID'),
    'arguments' => array(
      'id' => t('The activity ID'),
    ),
    'examples' => array(
      'Argument example' => 'drush garmin-connector-purge-activity 1142234',
    ),
  );
  $items['garmin-connector-list-activities'] = array(
    'description' => t('List the last x activities'),
    'arguments'   => array(
      'number'    => t('The number of activities to list'),
    ),
    'examples' => array(
      'Argument example' => 'drush garmin-connector-list-activities 10',
    ),
  );
  $items['garmin-connector-test-login'] = array(
    'description' => t('Test if Garmin Connector is logged into Garmin Connect.'),
    'examples' => array(
      'Standard example' => 'drush garmin-connector-test-login',
    ),
  );
  $items['garmin-connector-rebuild-system-catalogue'] = array(
    'description' => t('Rebuild the garmin connector system catalogue'),
    'examples' => array(
      'Standard example' => 'drush garmin-connector-rebuild-system-catalogue',
    ),
  );
  $items['garmin-connector-purge-untitled'] = array(
    'description' => t('Purge activities that have the title "Untitled"'),
    'arguments' => array(
      'from' => t('The earliest date'),
      'to' => t('The latest date'),
    ),
    'examples' => array(
      'Standard Example' => 'drush garmin-connector-purge-untitled (defaults to the last 30 days)',
      'Argument example' => 'drush garmin-connector-purge-untitled 2013-01-01 2013-07-07',
    ),
  );
  return $items;
}
/**
 * Callback function for drush garmin_connector_test_login.
 */
function drush_garmin_connector_test_login() {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (!isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  $debug = fopen('php://stdout', 'w');
  $ch = garmin_connector_initialize($debug);
  $status = garmin_connector_gc_is_logged_in($ch);
  if ($status == FALSE) {
    fwrite($debug, t("Since the status was not logged in, trying to log in now.\n"));
    garmin_connector_gc_login($ch, $debug);
    $status = garmin_connector_gc_is_logged_in($ch);
  }
  fwrite($debug, t("The current logged in status is this") . "(" . ($status == TRUE ?
  "true" : "false") . ")");
}


/**
 * Callback function for drush garmin_connector_sample_data.
 */
function drush_garmin_connector_sample_data() {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (!isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  $debug = fopen('php://stdout', 'w');
  garmin_connector_install_sample_data($debug);
}


/**
 * Callback function for drush garmin_connector_purge_untitled.
 *
 * Purge activities that have the title "Untitled". It's expected that you will
 * have gone into Garmin Connect and renamed them and want to force a refresh.
 *
 * @param string $from
 *   The earliest date to select purgeable activities
 *
 * @param string $to
 *   The latest date to select purgeable activities.
 */
function drush_garmin_connector_purge_untitled($from = NULL, $to = NULL) {
  if ($from == NULL) {
    $from = "one month ago";
  }
  if ($to == NULL) {
    $to = "today";
  }
  $f = garmin_connector_get_date($from);
  $t = garmin_connector_get_date($to);
  garmin_connector_verify_user();
  drush_print('You ran this command from drush. I\'m enabling special output');
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  $debug = fopen('php://stdout', 'w');
  garmin_connector_purge_untitled($f, $t, $debug);
  /* Now merge the activities into the catalog */
}

/**
 * Callback function for drush garmin_connector_retrieve.
 *
 * Retrieve activities from Garmin Connect. Adds them to the
 * garmin connector system catalog
 *
 * @param string $from
 *   The earliest date to retrieve
 *
 * @param string $to
 *   The latest date to retrieve
 */
function drush_garmin_connector_retrieve($from = NULL, $to = NULL) {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  if ($from == NULL) {
    $from = "one month ago";
  }
  if ($to == NULL) {
    $to = "today";
  }
  $f = garmin_connector_get_date($from);
  $t = garmin_connector_get_date($to);
  garmin_connector_verify_user();
  drush_print('You ran this command from drush. I\'m enabling special output');
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  $debug = fopen('php://stdout', 'w');
  fwrite($debug, "Getting activities from $f to $t\n");
  $activities = garmin_connector_get_activities_from_gc($debug, $f, $t);
  foreach ($activities as $activity) {
    $was_downloaded = garmin_connector_check_activity(
      $activity,
      $debug,
      NULL);
    if (!isset($was_downloaded)) {
      // This suppresses a warning from pareview.
    }
  }
  garmin_connector_rebuild_catalogue();
  /* Now merge the activities into the catalog */
}

/**
 * Callback function for drush garmin_connector_activities.
 */
function drush_garmin_connector_retreieve_activities() {
  garmin_connector_verify_user();
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  drush_print('You ran this command from drush. I\'m enabling special output');
  garmin_connector_retreieve_activities();
}

/**
 * Verify that the user can write to the garmin connector filesystem.
 *
 * If they cannot, attmempt to proceed
 */
function garmin_connector_verify_user() {
  // First make sure that this person is running as the same person who owns
  // files.
  $dir = "public://garmin_connector";
  $path = drupal_realpath($dir);
  $process_user = posix_getpwuid(posix_geteuid());
  $process_name = $process_user['name'];
  $public_user = posix_getpwuid(fileowner($path));
  $public_name = $public_user['name'];
  if ($process_name == $public_name) {
    // Sweet!
  }
  else {
    drush_print("The directory $dir is not writable. You are running drush as $process_name but the directory is owned by $public_name. It's likely that you should be running drush within sudo. For example: sudo -u $public_name drush ...  I'll forge ahead anyway but things will probably fail. Note also that any files will be created with the wrong owner and that may cause problems later.");
  }
}

/**
 * Callback for garmin_connector_refresh_activity.
 *
 * Because this will rewrite the system catalogue, you should be using drush
 * as the same user that the webserver runs as.
 *
 * @param int $id
 *   The id of the activity you want to refresh
 *
 * @throws Exception
 *   - When $id is missing.
 */
function drush_garmin_connector_refresh_activity($id) {
  if (!isset($id)) {
    throw new Exception(t("You must provide an id as an argument"));
  }
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  garmin_connector_verify_user();
  if (is_int($id) == TRUE) {
    drush_print("($id) needs to be an int\n");
    return;
  }
  garmin_connector_refresh_activity($id);
}

/**
 * Callback for garmin_connector_purge_activity.
 *
 * Because this will rewrite the system catalogue, you should be using drush
 * as the same user that the webserver runs as.
 *
 * @param int $id
 *   The id of the activity you want to purge
 */
function drush_garmin_connector_purge_activity($id) {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  garmin_connector_verify_user();
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  if (is_int($id) == TRUE) {
    drush_print("($id) needs to be an int\n");
    return;
  }
  garmin_connector_purge_activities(array($id));
}

/**
 * Callback for garmin_connector_list_activities.
 *
 * Note that no permissions need to be used here.
 *
 * @param int $number
 *   The number of activities to list
 */
function drush_garmin_connector_list_activities($number = 999) {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  $activities = garmin_connector_get_activities_from_local();
  drush_print(dt('These are the activities in the Garmin Connector catalogue:'));
  $count = 0;
  foreach ($activities as $value) {
    $count++;
    if ($count > $number) {
      break;
    }
    $time = new DateTime($value->activitySummary->BeginTimestamp->value);
    $message = sprintf("ID(%10s) Name(%10.10s) Type(%10.10s) Done(%19s) Duration(%10.10s)",
     $value->activityId,
     $value->activityName,
     $value->activityType->display,
     $time->format('Y-m-d H:i:s'),
     $value->activitySummary->SumElapsedDuration->display
    );
    drush_print($message);
  }
}

/**
 * Callback for garmin_connector_rebuild_system_catalogue.
 *
 * The user should run drush as the web server user.
 */
function drush_garmin_connector_rebuild_system_catalogue() {
  module_load_include('inc', 'garmin_connector', 'garmin_connector.catalogue');
  garmin_connector_verify_user();
  global $_garmin_connector_from_drush;
  $_garmin_connector_from_drush = 1;
  if (!isset($_garmin_connector_from_drush)) {
    // This suppresses a warning from pareview.
  }
  garmin_connector_rebuild_catalogue();
}
